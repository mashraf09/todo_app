package models

import (
	"gorm.io/gorm"
	"time"
)

type User struct {
	gorm.Model
	Name        string `json:"name" gorm:"column:name"`
	Username    string `json:"username" gorm:"unique;column:username"`
	Email       string `json:"email" gorm:"unique;column:email"`
	Password    string `json:"password" gorm:"column:password"`
	IsActive    bool   `json:"isActive" gorm:"column:is_active;default:false"`
	IsOauth     bool   `json:"isOauth "  gorm:"column:is_oauth;default:false"`
	SignupToken string `json:"signupToken" gorm:"column:signup_token"`
}

type Task struct {
	gorm.Model
	Title            string    `json:"title" gorm:"column:title"`
	Description      string    `json:"description" gorm:"column:description" `
	DueAt            time.Time `json:"dueAt" gorm:"column:due_at""`
	CompletionAt     time.Time `json:"CompletionAt" gorm:"column:completion_at"`
	CompletionStatus string    `json:"completionStatus" gorm:"column:completion_status"`
	ListID           uint      `json:"listID" gorm:"column:list_id"`
	List             List      `gorm:"foreignKey:ListID;constraint:OnDelete:CASCADE"`
}

type List struct {
	gorm.Model
	Title  string `json:"title" gorm:"column:title"`
	UserID uint   `json:"userID" gorm:"column:user_id"`
	User   User   `gorm:"foreignKey:UserID;constraint:OnDelete:CASCADE"`
}

type UserToken struct {
	gorm.Model
	Token         string    `json:"token" gorm:"column:token"`
	UserID        uint      `json:"userID" gorm:"column:user_id"`
	User          User      `gorm:"foreignKey:UserID;constraint:OnDelete:CASCADE""`
	TokenExpiryAT time.Time `json:"token_expiry_at" gorm:"column:token_expiry"`
}

type TaskAttachments struct {
	gorm.Model
	TaskID     uint      `json:"taskID" gorm:"column:task_id"`
	Task       Task      `gorm:"foreignKey:TaskID;constraint:OnDelete:CASCADE"`
	Path       string    `json:"path" gorm:"column:path"`
	Name       string    `json:"name" gorm:"column:name"`
	UploadedAt time.Time `json:"uploadedAt" gorm:"column:uploaded_at"`
}

type TokenVerify struct {
	Email string `json:"email"`
	Token string `json:"token"`
}

type Response struct {
	Status string      `json:"status"`
	Error  string      `json:"error"`
	Data   interface{} `json:"data"`
}

type TokenRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type ResetPassword struct {
	Email       string `json:"email"`
	Token       string `json:"token"`
	NewPassword string `json:"new_password"`
}
