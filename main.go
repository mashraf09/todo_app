package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/uber-go/tally"
	"go.uber.org/cadence/.gen/go/cadence/workflowserviceclient"
	"go.uber.org/cadence/worker"
	"todo/backgroundTasks"
	"todo/cadenceWorkflows"
	"todo/controllers"
	database "todo/databases"
	"todo/docs"
	"todo/env"
	"todo/handlers"
)

var TaskListName = env.Env.CadenceTaskListName
var Domain = env.Env.CadenceDomain

func main() {
	startWorker(cadenceWorkflows.BuildCadenceClient())
	dbHost := env.Env.DbHost
	dbPort := env.Env.DbPort
	dbUserName := env.Env.DbUsername
	dbName := env.Env.DbName
	dbPassword := env.Env.DbPassword
	dbSSLMode := env.Env.DbSSLMode

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s",
		dbHost, dbUserName, dbPassword, dbName, dbPort, dbSSLMode)
	database.Connect(dsn)
	database.Migrate()

	go backgroundTasks.ExecuteCronJob()

	router := gin.Default()

	docs.SwaggerInfo.BasePath = "/api"
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	handlers.InitOauthConfig()

	controllers.UserController(router.RouterGroup)
	controllers.TaskController(router.RouterGroup)
	controllers.ListController(router.RouterGroup)
	controllers.ReportController(router.RouterGroup)
	controllers.AttachmentController(router.RouterGroup)
	controllers.OauthController(router.RouterGroup)
	router.Run(env.Env.ServerPort)

}

func startWorker(service workflowserviceclient.Interface) {
	// TaskListName identifies set of client workflows, activities, and workers.
	// It could be your group or client or application name.
	workerOptions := worker.Options{
		MetricsScope: tally.NewTestScope(cadenceWorkflows.ApplicationName, map[string]string{}),
	}

	worker := worker.New(
		service,
		Domain,
		cadenceWorkflows.ApplicationName,
		workerOptions)

	worker.RegisterWorkflow(cadenceWorkflows.EmailNotifyWorkflow)
	worker.RegisterActivity(cadenceWorkflows.SendEmail)

	err := worker.Start()
	if err != nil {
		panic("Failed to start worker")
	}
	log.Info("Started Worker.", "worker", TaskListName)
}
