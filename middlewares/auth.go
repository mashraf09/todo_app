package middlewares

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
	"todo/auth"
	database "todo/databases"
	"todo/env"
	"todo/utility"
)

func Auth() gin.HandlerFunc {
	return func(context *gin.Context) {
		log.Info("Auth() Function called")
		tokenString := context.GetHeader("Authorization")

		if tokenString == "" {

			err := "request does not contain an access token"
			log.Error(err)
			utility.BuildResponse(context, http.StatusUnauthorized, "error", err, nil)
			return
		}

		user, err := database.VerifyToken(tokenString)
		if err != nil {
			log.Error(err.Error())
			if err.Error() != "record not found" {
				utility.BuildResponse(context, http.StatusUnauthorized, "error", err.Error(), nil)
				return
			}
			utility.BuildResponse(context, http.StatusUnauthorized, "error", "No User found", nil)
			return

		}

		var UserEmail string

		if !user.IsOauth {
			err := auth.ValidateToken(tokenString)
			if err != nil {
				log.Error(err.Error())
				utility.BuildResponse(context, http.StatusUnauthorized, "error", err.Error(), nil)
				return
			}

			claims := jwt.MapClaims{}
			token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
				return []byte(env.Env.SecreteKey), nil
			})
			if err == nil && token.Valid {
				UserEmail = claims["email"].(string)
			}

		} else {

			resp, err := http.Get(env.Env.OauthTokenInfoUrl + tokenString)
			if err != nil {
				log.Error(err.Error())
				utility.BuildResponse(context, http.StatusBadRequest, "error", "", err.Error())
				return
			}
			if resp.StatusCode != 200 {
				log.Error("Invalid Token Value")
				utility.BuildResponse(context, http.StatusUnauthorized, "error", "Invalid Token Value", nil)
				return
			}

		}

		UserEmail = user.Email
		context.Request.Header.Set("Email", UserEmail)
		UserEmail = context.GetHeader("Email")
		id, err := database.GetUserID(UserEmail)
		if err != nil {
			log.Error(err.Error())
			return
		}
		ID := int(id)
		UserID := strconv.Itoa(ID)
		context.Request.Header.Set("UserID", UserID)

		log.Info("Authentication Successful")
		context.Next()
	}
}
