package cache

import (
	"github.com/patrickmn/go-cache"
	"todo/utility"
)

var CacheObj *cache.Cache

func init() {

	CacheObj = cache.New(utility.CacheExpireyTime, utility.CacheCleanUpInterval)

}
