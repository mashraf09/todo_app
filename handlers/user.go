package handlers

import (
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
	"todo/auth"
	database "todo/databases"
	"todo/models"
	"todo/utility"
)

// RegisterUser
// @Summary      Register User
// @Description  Get user information
// @Tags         Users
// @Accept       json
// @Produce      json
// @Param 		 User body models.User true "User Information" Format(models.User)
// @Success      200  {object}   models.Response
// @Failure      400  {object}  models.Response
// @Failure      401  {object}  models.Response
// @Failure      500  {object}  models.Response
// @Router       /users/signup [Post]
func RegisterUser(context *gin.Context) {

	log.Info("RegisterUser Handler Function Called")

	var user models.User
	if err := context.ShouldBindJSON(&user); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", "Error", nil)
		return
	}
	if err := user.HashPassword(user.Password); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", "Error", nil)
		return
	}
	tokenString, err := auth.GenerateJWT(user.Email, user.Username)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", "Error", nil)
		return
	}
	user.SignupToken = tokenString
	err = database.CreateUser(&user)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", err.Error(), nil)

		return
	}

	utility.AccountVerificationMail(user.Email, tokenString)
	utility.BuildResponse(context, http.StatusCreated, "Success: User created", "", user)
	log.Info("Success: User created")
}

// VerifyUser
// @Summary      Verify User
// @Description  Verifies user using signup token
// @Tags         Users
// @Accept       json
// @Produce      json
// @Param        TokenVerify    body     models.TokenVerify true  "User credentials"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      401  {object}  models.Response
// @Failure      500  {object}  models.Response
// @Router       /users/verify [Post]
func VerifyUser(context *gin.Context) {
	log.Info("VerifyUser Handler Function Called ")

	var request models.TokenVerify
	var user models.User
	if err := context.ShouldBindJSON(&request); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", err.Error(), nil)
		return
	}

	er := database.VerifyUser(&user, request.Email)
	if er != nil {
		log.Error(er.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", er.Error(), nil)
		return
	}
	tokenString := request.Token
	if tokenString == "" {
		log.Error("Error: request does not contain an access token")
		utility.BuildResponse(context, http.StatusUnauthorized, "", "request does not contain an access token", nil)
		return
	}
	err := auth.ValidateToken(tokenString)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusUnauthorized, "Error", err.Error(), nil)

		return
	}
	user.SignupToken = "n/a"
	user.IsActive = true
	err = database.UpdateUser(&user)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusUnauthorized, "", err.Error(), nil)
		return
	}

	utility.BuildResponse(context, http.StatusOK, "Success: Account Verified", "", nil)
	log.Info("Success: Account Verified")
}

// LoginUser
// @Summary      Login User
// @Description  User logged in and a token returned as response
// @Tags         Users
// @Accept       json
// @Produce      json
// @Param        TokenRequest    body     models.TokenRequest true  "User credentials"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      401  {object}  models.Response
// @Failure      500  {object}  models.Response
// @Router       /users/login [Post]
func LoginUser(context *gin.Context) {
	log.Info("LoginUser Handler Function Called")

	var request models.TokenRequest
	var user models.User
	if err := context.ShouldBindJSON(&request); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", err.Error(), nil)
		return
	}

	user, er := database.VerifyCredentials(request.Email, request.Password)
	if er != nil {
		log.Error(er.Error())
		utility.BuildResponse(context, http.StatusUnauthorized, "", er.Error(), nil)
		return
	}

	tokenString, err := auth.GenerateJWT(request.Email, request.Password)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", err.Error(), nil)
		return
	}

	var userToken models.UserToken
	userToken.UserID = user.ID
	userToken.Token = tokenString
	tokenError := database.AssignToken(&userToken)
	if tokenError != nil {
		log.Error(tokenError.Error())
		utility.BuildResponse(context, http.StatusUnauthorized, "", tokenError.Error(), nil)
		return
	}

	utility.BuildResponse(context, http.StatusOK, "Success: Login", "", tokenString)
	log.Info("Login Successful")

}

// ForgotPassword
// @Summary      Forgot Password
// @Description  Takes email as input to reset user password
// @Tags         Users
// @Accept       json
// @Produce      json
// @Param 		 email 	body  string	true "User Email"
// @Success      200  {object}   models.Response
// @Failure      400  {object}  models.Response
// @Failure      401  {object}  models.Response
// @Failure      500  {object}  models.Response
// @Router       /users/forgot_password [Post]
func ForgotPassword(context *gin.Context) {
	log.Info("ForgotPassword Handler Function Called")

	var user models.User
	if err := context.ShouldBindJSON(&user); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", "Error", nil)
		return
	}

	er := database.VerifyUser(&user, user.Email)
	if er != nil {
		log.Error(er.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", er.Error(), nil)
		return
	}

	if user.IsOauth {
		utility.BuildResponse(context, http.StatusForbidden, "Password reset is not allowed for OAuth Users", "", nil)
		return
	}

	tokenString, err := auth.GenerateJWT(user.Email, user.Username)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", "Error", nil)
		return
	}

	err = utility.ResetPasswordMail(context, user.Email, tokenString)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "Email sent failed", err.Error(), nil)
		return
	}

	utility.BuildResponse(context, http.StatusCreated, "Success: Reset password email sent", "", user)
	log.Info("Success: Reset password email sent")
}

// ResetPassword
// @Summary      Reset Password
// @Description  Resets users password provided new credentials and reset token
// @Tags         Users
// @Accept       json
// @Produce      json
// @Param        ResetPassword    body     models.ResetPassword true  "New password"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      401  {object}  models.Response
// @Failure      500  {object}  models.Response
// @Router       /users/reset_password [Post]
func ResetPassword(context *gin.Context) {
	log.Info("ResetPassword Handler Function Called ")

	var request models.ResetPassword
	var user models.User
	if err := context.ShouldBindJSON(&request); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", err.Error(), nil)
		return
	}

	er := database.VerifyUser(&user, request.Email)
	if er != nil {
		log.Error(er.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", er.Error(), nil)
		return
	}
	tokenString := request.Token
	if tokenString == "" {
		log.Error("Error: request does not contain an access token")
		utility.BuildResponse(context, http.StatusUnauthorized, "", "request does not contain an access token", nil)
		return
	}
	err := auth.ValidateToken(tokenString)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusUnauthorized, "Error", err.Error(), nil)

		return
	}

	if err := user.HashPassword(request.NewPassword); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", "Error", nil)
		return
	}
	err = database.UpdateUser(&user)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusUnauthorized, "", err.Error(), nil)
		return
	}

	utility.BuildResponse(context, http.StatusOK, "Success: Password reset", "", nil)
	log.Info("Success: Password reset")
}
