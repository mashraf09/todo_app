package handlers

import (
	mapset "github.com/deckarep/golang-set/v2"
	"github.com/gin-gonic/gin"
	Cache "github.com/patrickmn/go-cache"
	log "github.com/sirupsen/logrus"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"todo/cache"
	database "todo/databases"
	"todo/utility"
)

// CountStats
// @Summary      Report: Count Stats
// @Description  Report returns count of completed, non-completed and total tasks.
// @Tags         Reports
// @Accept       json
// @Produce      json
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Router       /reports/count_stats [Get]
func CountStats(context *gin.Context) {
	log.Info("CountStats Handler Called")

	resp, found := cache.CacheObj.Get("CountStats")
	if found {
		log.Info("Results returned from cache for : CountStats")
		utility.BuildResponse(context, http.StatusOK, "Success: CountStats Report Generated", "", resp)
		return
	}

	var results []map[string]interface{}
	id := context.GetHeader("UserID")
	userid, _ := strconv.Atoi(id)
	UserID := uint(userid)

	results, err := database.CountStats(UserID)
	if err != nil {
		log.Info(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "Error", err.Error(), nil)
		return
	}

	total := make(map[string]interface{})
	var sum int64
	for i := 0; i < len(results); i++ {
		sum = sum + results[i]["count"].(int64)
	}

	total["total"] = sum
	results = append(results, total)

	cache.CacheObj.Set("CountStats", results, Cache.DefaultExpiration)
	utility.BuildResponse(context, http.StatusOK, "Success: CountStats Report Generated", "", results)
	log.Info("Success: CountStats Report Generated")
	return
}

// AverageTasksCompletedPerDay
// @Summary      Report: Average tasks completed on daily basis
// @Description  Report returns average of task completed daily since time of account creation .
// @Tags         Reports
// @Accept       json
// @Produce      json
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Router       /reports/completion_average [Get]
func AverageTasksCompletedPerDay(context *gin.Context) {

	log.Info("AvgTasksCompletedPerDay Handler Called")

	resp, found := cache.CacheObj.Get("AverageTasksCompletedPerDay")
	if found {
		log.Info("Results returned from cache for : AverageTasksCompletedPerDay")
		utility.BuildResponse(context, http.StatusOK, "Success: AverageTasksCompletedPerDay Report Generated", "", resp)
		return
	}

	var results map[string]interface{}
	id := context.GetHeader("UserID")
	userid, _ := strconv.Atoi(id)
	UserID := uint(userid)

	results, err := database.AverageTasksCompletedPerDay(UserID)
	if err != nil {
		log.Info(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "Error", err.Error(), nil)
		return
	}
	cache.CacheObj.Set("AverageTasksCompletedPerDay", results, Cache.DefaultExpiration)
	utility.BuildResponse(context, http.StatusOK, "Success: AverageTasksCompletedPerDay Report Generated", "", results)
	log.Info("Success: AverageTasksCompletedPerDay Report Generated")
	return
}

// DelayedTasks
// @Summary      Report: Delayed Tasks
// @Description  Report returns Count of tasks which could not be completed on time .
// @Tags         Reports
// @Accept       json
// @Produce      json
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Router       /reports/delayed_tasks [Get]
func DelayedTasks(context *gin.Context) {
	log.Info("DelayedTasks Handler Called")

	resp, found := cache.CacheObj.Get("DelayedTasks")
	if found {
		log.Info("Results returned from cache for : DelayedTasks")
		utility.BuildResponse(context, http.StatusOK, "Success: DelayedTasks Report Generated", "", resp)
		return
	}

	//var results []map[string]interface{}
	id := context.GetHeader("UserID")
	userid, _ := strconv.Atoi(id)
	UserID := uint(userid)

	results, err := database.DelayedTasks(UserID)
	if err != nil {
		log.Info(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "Error", err.Error(), nil)
		return
	}
	cache.CacheObj.Set("DelayedTasks", results, Cache.DefaultExpiration)

	utility.BuildResponse(context, http.StatusOK, "Success: DelayedTasks Report Generated", "", results)
	log.Info("Success: DelayedTasks Report Generated")
	return
}

// MaxTasksCompletedInSingleDay
// @Summary      Report: Max Tasks Completed In SingleDay
// @Description  Since time of account creation, on what date, maximum number of tasks were completed in a single day.
// @Tags         Reports
// @Accept       json
// @Produce      json
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Router       /reports/max_completions_in_day [Get]
func MaxTasksCompletedInSingleDay(context *gin.Context) {
	log.Info("MaxTasksCompletedInSingleDay Handler Called")

	resp, found := cache.CacheObj.Get("MaxTasksCompletedInSingleDay")
	if found {
		log.Info("Results returned from cache for : MaxTasksCompletedInSingleDay")
		utility.BuildResponse(context, http.StatusOK, "Success: MaxTasksCompletedInSingleDay Report Generated", "", resp)
		return
	}

	var results []map[string]interface{}
	id := context.GetHeader("UserID")
	userid, _ := strconv.Atoi(id)
	UserID := uint(userid)

	results, err := database.MaxTasksCompletedInSingleDay(UserID)
	if err != nil {
		log.Info(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "Error", err.Error(), nil)
		return
	}

	cache.CacheObj.Set("MaxTasksCompletedInSingleDay", results, Cache.DefaultExpiration)
	utility.BuildResponse(context, http.StatusOK, "Success: MaxTasksInSingleDay Report Generated", "", results)
	log.Info("Success: MaxTasksInSingleDay Report Generated")
	return
}

// TasksCreationStats
// @Summary      Report: Tasks Creation Stats
// @Description  Since time of account creation, how many tasks are opened on every day of the week (mon, tue, wed, ....).
// @Tags         Reports
// @Accept       json
// @Produce      json
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Router       /reports/tasks_creation_stats [Get]
func TasksCreationStats(context *gin.Context) {
	log.Info("TasksCreationStats Handler Called")

	resp, found := cache.CacheObj.Get("TasksCreationStats")
	if found {
		log.Info("Results returned from cache for : TasksCreationStats")
		utility.BuildResponse(context, http.StatusOK, "Success: TasksCreationStats Report Generated", "", resp)
		return
	}

	var results []map[string]interface{}
	id := context.GetHeader("UserID")
	userid, _ := strconv.Atoi(id)
	UserID := uint(userid)

	results, err := database.TasksCreationStats(UserID)
	if err != nil {
		log.Info(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "Error", err.Error(), nil)
		return
	}

	cache.CacheObj.Set("TasksCreationStats", results, Cache.DefaultExpiration)

	utility.BuildResponse(context, http.StatusOK, "Success: TasksCreationStats Report Generated", "", results)
	log.Info("Success: TasksCreationStats Report Generated")
	return
}
func SimilarTaskStats(context *gin.Context) {
	log.Info("SimilarTaskStats Handler Called")

	resp, found := cache.CacheObj.Get("SimilarTaskStats")
	if found {
		log.Info("Results returned from cache for : SimilarTaskStats")
		utility.BuildResponse(context, http.StatusOK, "Success: SimilarTaskStats Report Generated", "", resp)
		return
	}

	id := context.GetHeader("UserID")
	userid, _ := strconv.Atoi(id)

	UserID := uint(userid)

	results, err := database.SimilarTaskStats(UserID)
	if err != nil {
		log.Info(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "Error", err.Error(), nil)
		return
	}

	sort.Slice(results, func(i, j int) bool {
		return len(results[i]) < len(results[j])
	})
	var results_set []mapset.Set[string]

	for _, element := range results {

		tokenizedTask := strings.Fields(element)
		required := mapset.NewSet[string]()
		for i := 0; i < len(tokenizedTask); i += 1 {
			required.Add(tokenizedTask[i])
		}

		results_set = append(results_set, required)

	}
	var similarTasksList []map[string]mapset.Set[string]

	for i := 0; i < len(results_set); i++ {

		new := results_set[i+1:]
		for j := 0; j < len(new); j++ {

			if results_set[i].IsSubset(new[j]) {
				m := map[string]mapset.Set[string]{"task": results_set[i], "matched_task": new[j]}
				similarTasksList = append(similarTasksList, m)

			}

		}

	}

	cache.CacheObj.Set("SimilarTaskStats", similarTasksList, Cache.DefaultExpiration)

	utility.BuildResponse(context, http.StatusOK, "Success: SimilarTaskStats Report Generated", "", similarTasksList)
	log.Info("Success: SimilarTaskStats Report Generated")
	return
}
