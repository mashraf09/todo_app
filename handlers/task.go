package handlers

import (
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
	database "todo/databases"
	"todo/models"
	"todo/utility"
)

// CreateTask
// @Summary      Create Task
// @Description  Creates a task
// @Tags         Tasks
// @Accept       json
// @Produce      json
// @Param        task    body     models.Task true  "task details"
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      500  {object}  models.Response
// @Router       /tasks [Post]
func CreateTask(context *gin.Context) {
	log.Info("CreateTask Handler Function Called ")
	var task models.Task
	var UserEmail string
	if err := context.ShouldBindJSON(&task); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", "Error", err.Error())
		return
	}
	UserEmail = context.GetHeader("Email")

	listObj, err := database.CheckTaskList(UserEmail)
	if err != nil {
		log.Error("List Not Found, Please create list first")
		utility.BuildResponse(context, http.StatusInternalServerError, "List Not Found, Please create list first", err.Error(), nil)
		return
	}

	task.ListID = listObj.ID

	err = database.CreateTask(&task)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", err.Error(), listObj)
		return
	}

	utility.BuildResponse(context, http.StatusOK, "Success: Task created", "", task)
	log.Info("Task Created Successfully")

}

// DeleteTask
// @Summary      Delete Task
// @Description  Deletes a task
// @Tags         Tasks
// @Accept       json
// @Produce      json
// @Param        task    body     string true  "taskID"
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Router       /tasks/{id} [Delete]
func DeleteTask(context *gin.Context) {
	log.Info("DeleteTask Handler Function Called")
	var task models.Task

	if err := context.ShouldBindJSON(&task); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", "Error", err.Error())
		return
	}

	err := database.DeleteTask(&task)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", err.Error(), nil)
		return
	}

	taskID := task.ID

	err = utility.DeleteAttachments(taskID)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", err.Error(), nil)
		return
	}
	utility.BuildResponse(context, http.StatusOK, "Success: Task Deleted", "", nil)
	log.Info("Task Deleted Successfully")

}

// UpdateTask
// @Summary      Update Task
// @Description  Updates tasks information
// @Tags         Tasks
// @Accept       json
// @Produce      json
// @Param        task    body     models.Task true  "taskID"
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Router       /tasks/{id} [Put]
func UpdateTask(context *gin.Context) {
	log.Info("UpdateTask Handler Function Called")
	var task models.Task

	if err := context.ShouldBindJSON(&task); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", "Error", err.Error())
		return
	}
	err := database.UpdateTask(&task)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", err.Error(), nil)
		return
	}
	utility.BuildResponse(context, http.StatusOK, "Success: Task Updated", "", task)
	log.Info("Task Updated Successfully")

}
