package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"io/ioutil"
	"net/http"
	database "todo/databases"
	"todo/env"
	"todo/models"
	"todo/utility"
)

var (
	googleOauthConfig = &oauth2.Config{
		RedirectURL:  "",
		ClientID:     "",
		ClientSecret: "",
		Scopes:       []string{},
		Endpoint:     google.Endpoint,
	}
	randomState = "random"
)

func InitOauthConfig() {
	googleOauthConfig.RedirectURL = env.Env.OauthRedirectUrl
	googleOauthConfig.ClientID = env.Env.OauthClientID
	googleOauthConfig.ClientSecret = env.Env.OauthClientSecrete
	googleOauthConfig.Scopes = utility.OauthScopes
	googleOauthConfig.Endpoint = google.Endpoint
	randomState = "random"

}

func OauthHome(context *gin.Context) {
	var html = `<html><body><a href="/api/oauth/login_oauth">Google Log IN</a></body></html>`
	fmt.Fprint(context.Writer, html)
}

func HandleLogin(context *gin.Context) {

	url := googleOauthConfig.AuthCodeURL(randomState)
	context.Redirect(http.StatusTemporaryRedirect, url)

}
func HandleCallback(context *gin.Context) {

	fmt.Println(googleOauthConfig, context.Request.FormValue("stage"))
	if context.Request.FormValue("state") != randomState {
		log.Error("state is no valid")
		context.Redirect(http.StatusTemporaryRedirect, "/api/oauth")
		return

	}

	token, err := googleOauthConfig.Exchange(context, context.Request.FormValue("code"))
	if err != nil {
		log.Error("could not get token", err.Error())
		context.Redirect(http.StatusTemporaryRedirect, "/api/oauth")
		return
	}

	resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		log.Error("could not create get request%s\n", err.Error())
		context.Redirect(http.StatusTemporaryRedirect, "/api/oauth")
		return
	}

	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error("could not parse response:%s\n", err.Error())
		context.Redirect(http.StatusTemporaryRedirect, "/api/oauth")
		return
	}
	// Convert bytes to string.
	var userInfo map[string]interface{}
	json.Unmarshal(content, &userInfo)

	userInfo["token"] = token
	var userToken models.UserToken

	user, err := database.IsUserExist(userInfo["email"].(string))
	if err != nil {

		log.Error(err.Error())
		if err.Error() == "record not found" {
			fmt.Println("ID with zero", user.ID)
			user.Username = userInfo["email"].(string)
			user.Name = userInfo["name"].(string)
			user.IsOauth = true
			user.IsActive = true
			user.Email = userInfo["email"].(string)
			user.SignupToken = ""
			//
			//	log.Info(user)
			err = database.CreateUser(&user)
			if err != nil {
				log.Error(err.Error())
				utility.BuildResponse(context, http.StatusInternalServerError, "", err.Error(), nil)
				return
			}
			log.Info("User Created Successfully")
			_, err := database.GetUserID(user.Email)
			if err != nil {
				log.Error(err.Error())
				return
			}

		}
	}
	userToken.UserID = user.ID
	userToken.Token = token.AccessToken
	userToken.TokenExpiryAT = token.Expiry
	tokenError := database.AssignToken(&userToken)
	if tokenError != nil {
		log.Error(tokenError.Error())
		utility.BuildResponse(context, http.StatusUnauthorized, "", tokenError.Error(), nil)
		return
	}
	utility.BuildResponse(context, http.StatusOK, "Success", "", token)
	return

}
