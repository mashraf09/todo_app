package handlers

import (
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
	database "todo/databases"
	"todo/models"
	"todo/utility"
)

// CreateList
// @Summary      Create List
// @Description  Creates a list
// @Tags         Lists
// @Accept       json
// @Produce      json
// @Param        "title"    body    models.List true  "list title"
// @Param        Authorization  header    string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Router       /lists [Post]
func CreateList(context *gin.Context) {
	log.Info("CreateList Handler Function Called")

	var list models.List
	var UserEmail string
	if err := context.ShouldBindJSON(&list); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", "Error", err.Error())
		return
	}

	UserEmail = context.GetHeader("Email")

	status, title, dberr := database.CreateList(&list, UserEmail)
	if dberr != nil {
		log.Error(dberr.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", dberr.Error(), nil)
		return
	}
	utility.BuildResponse(context, http.StatusOK, status, "", title)
	log.Info(status)

}

// DeleteList
// @Summary      Delete List
// @Description  Deletes a list
// @Tags         Lists
// @Accept       json
// @Produce      json
// @Param        id    body    string true  "list"
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Router       /lists/{id} [Delete]
func DeleteList(context *gin.Context) {
	log.Info("DeleteList Handler Function Called")
	var list models.List
	if err := context.ShouldBindJSON(&list); err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", "Error", err.Error())
		return
	}
	err := database.DeleteList(&list)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", err.Error(), nil)
		return
	}
	utility.BuildResponse(context, http.StatusOK, "Success: List Deleted", "", nil)
	log.Info("List Deleted Successfully")
}

// GetList
// @Summary      View List
// @Description  Returns a tasks list
// @Tags         Lists
// @Accept       json
// @Produce      json
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Router       /lists [Get]
func GetList(context *gin.Context) {

	log.Info("GetList Handler Function Called")
	var UserEmail string
	UserEmail = context.GetHeader("Email")

	var tasks []models.Task
	err := database.ViewList(&tasks, UserEmail)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusBadRequest, "", err.Error(), nil)
		return
	}
	utility.BuildResponse(context, http.StatusOK, "Success", "", tasks)
	log.Info("Successfully returned List")
}
