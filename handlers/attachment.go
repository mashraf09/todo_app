package handlers

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	database "todo/databases"
	"todo/env"
	"todo/models"
	"todo/utility"
)

// DownloadAttachment
// @Summary      Download Attachment
// @Description  Downloads the attachment
// @Tags         Attachments
// @Accept       json
// @Produce      json
// @Param        attach_id    query     string true  "attachment id"
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      500  {object}  models.Response
// @Router       /attachments [Get]
func DownloadAttachment(context *gin.Context) {
	log.Info("DownloadAttachment Handler Function Called")

	var taskAttachment models.TaskAttachments
	x := context.Request.URL.Query().Get("attach_id")
	y, _ := strconv.Atoi(x)
	TaskID := uint(y)

	err := database.DownloadAttachment(&taskAttachment, TaskID)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", err.Error(), nil)
		return
	}

	path := taskAttachment.Path
	segments := strings.Split(path, "/")
	fileName := segments[len(segments)-1]
	downloadPathSegment := segments[:len(segments)-1]
	downloadPath := strings.Join(downloadPathSegment, "/")

	targetPath := filepath.Join(downloadPath, fileName)
	if !strings.HasPrefix(filepath.Clean(targetPath), downloadPath) {
		log.Error(http.StatusForbidden)
		context.String(http.StatusForbidden, "Look like you attacking me")
		return
	}
	//Seems this headers needed for some browsers (for example without this headers Chrome will download files as txt)
	context.Header("Content-Description", "File Transfer")
	context.Header("Content-Transfer-Encoding", "binary")
	context.Header("Content-Disposition", "attachment; filename="+fileName)
	context.Header("Content-Type", "application/octet-stream")
	context.File(targetPath)
	log.Info("DownloadAttachment Successful")
	return
}

// UploadAttachment
// @Summary      Upload Attachment
// @Description  Uploads the attachments
// @Tags         Attachments
// @Accept       json
// @Produce      json
// @Param        task_id    query     string true  "task id"
// @Param        name    query    string true  "attachment name"
// @Param        myfile    formData     file true  "attachments"
// @Param        Authorization  header     string true  "token"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      500  {object}  models.Response
// @Router       /attachments [Post]
func UploadAttachment(context *gin.Context) {
	log.Info("UploadAttachment Handler Function Called")

	var taskAttachment []models.TaskAttachments

	taskIDStr := context.Request.URL.Query().Get("task_id")
	y, _ := strconv.Atoi(taskIDStr)
	taskID := uint(y)

	attachmentName := context.Request.URL.Query().Get("name")

	///to check if respective task exist or not
	err := database.CheckTask(taskID)
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "", err.Error(), nil)
		return
	}

	var dirPath = env.Env.AttachmentUploadPath + "task_" + taskIDStr
	if _, err := os.Stat(dirPath); errors.Is(err, os.ErrNotExist) {
		err := os.Mkdir(dirPath, os.ModePerm)
		if err != nil {
			log.Error(err.Error())
			utility.BuildResponse(context, http.StatusInternalServerError, "DIR-ERROR", err.Error(), nil)
			return
		}

	}

	context.Request.ParseMultipartForm(10 << 20)
	file := context.Request.MultipartForm.File["myfile"]
	for _, handler := range file {
		f, err := handler.Open()
		// f is one of the files
		if err != nil {
			log.Error(err.Error())
			utility.BuildResponse(context, http.StatusInternalServerError, "File-ERROR", err.Error(), nil)
			return
		}
		defer f.Close()

		var FileWritePath = fmt.Sprintf("%stask_%s/%s", env.Env.AttachmentUploadPath, taskIDStr, handler.Filename)

		dst, err := os.Create(FileWritePath)
		defer dst.Close()
		if err != nil {
			log.Error(err.Error())
			utility.BuildResponse(context, http.StatusInternalServerError, "FILE CREATE ERRO", err.Error(), nil)
			return
		}

		// Copy the uploaded file to the created file on the filesystem
		if _, err := io.Copy(dst, f); err != nil {
			log.Error(err.Error())
			utility.BuildResponse(context, http.StatusInternalServerError, "copy error", err.Error(), nil)
			return

		}
		taskAttachment = append(taskAttachment, models.TaskAttachments{TaskID: taskID, Name: attachmentName, Path: FileWritePath})
	}

	err = database.UploadAttachment(&taskAttachment, len(taskAttachment))
	if err != nil {
		log.Error(err.Error())
		utility.BuildResponse(context, http.StatusInternalServerError, "DB error", err.Error(), nil)
		return
	}

	utility.BuildResponse(context, http.StatusOK, "Successfully Uploaded a File", "", taskAttachment)
	log.Info("UploadAttachment Successful")
}
