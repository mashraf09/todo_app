package cadenceWorkflows

import (
	log "github.com/sirupsen/logrus"
	"go.uber.org/cadence/.gen/go/cadence/workflowserviceclient"
	"go.uber.org/yarpc"
	"go.uber.org/yarpc/transport/tchannel"
	"todo/env"
)

var HostPort = env.Env.CadenceHostPort
var ClientName = env.Env.CadenceClient
var CadenceService = env.Env.CadenceService

func BuildCadenceClient() workflowserviceclient.Interface {
	log.Info("BuildCadenceClient Function Called")
	ch, err := tchannel.NewChannelTransport(tchannel.ServiceName(ClientName))
	if err != nil {
		panic("Failed to setup tchannel")
	}
	dispatcher := yarpc.NewDispatcher(yarpc.Config{
		Name: ClientName,
		Outbounds: yarpc.Outbounds{
			CadenceService: {Unary: ch.NewSingleOutbound(HostPort)},
		},
	})
	if err := dispatcher.Start(); err != nil {
		panic("Failed to start dispatcher")
	}
	return workflowserviceclient.New(dispatcher.ClientConfig(CadenceService))

}
