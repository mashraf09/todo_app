package cadenceWorkflows

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"go.uber.org/cadence/workflow"
	"go.uber.org/zap"
	"time"
)

const ApplicationName = "helloWorldGroup"

func EmailNotifyWorkflow(ctx workflow.Context, emailRecipients []string, from string, subject string, emailBody string, host string, password string, port string) error {
	log.Info("EmailNotifyWorkflow Called")
	ao := workflow.ActivityOptions{
		ScheduleToCloseTimeout: time.Second * 200,
		ScheduleToStartTimeout: time.Second * 200,
		StartToCloseTimeout:    time.Second * 200,
		HeartbeatTimeout:       time.Second * 200,
		WaitForCancellation:    false,
	}
	ctx = workflow.WithActivityOptions(ctx, ao)

	future := workflow.ExecuteActivity(ctx, SendEmail, emailRecipients, from, subject, emailBody, host, password, port)
	var result string
	if err := future.Get(ctx, &result); err != nil {
		return err
	}
	fmt.Println(result)
	workflow.GetLogger(ctx).Info("Done", zap.String("result", result))
	return nil
}
