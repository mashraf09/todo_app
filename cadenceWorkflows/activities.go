package cadenceWorkflows

import (
	"context"
	log "github.com/sirupsen/logrus"
	"net/smtp"
)

func SendEmail(ctx context.Context, emailRecipients []string, from string, subject string, emailBody string, host string, password string, port string) error {

	log.Info("SendEmail Activity Function Called")
	address := host + ":" + port
	message := []byte(subject + emailBody)

	auth := smtp.PlainAuth("", from, password, host)

	err := smtp.SendMail(address, auth, from, emailRecipients, message)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	return nil
}
