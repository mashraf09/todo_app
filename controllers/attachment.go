package controllers

import (
	"github.com/gin-gonic/gin"
	"todo/handlers"
	"todo/middlewares"
)

func AttachmentController(router gin.RouterGroup) {

	api := router.Group("/api/attachments").Use(middlewares.Auth())
	{
		api.POST("/", handlers.UploadAttachment)
		api.GET("/", handlers.DownloadAttachment)

	}

}
