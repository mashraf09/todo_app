package controllers

import (
	"github.com/gin-gonic/gin"
	"todo/handlers"
)

func OauthController(router gin.RouterGroup) {

	api := router.Group("/api/oauth")
	{
		api.GET("/", handlers.OauthHome)
		api.GET("/login_oauth", handlers.HandleLogin)
		api.GET("/callback", handlers.HandleCallback)

	}

}
