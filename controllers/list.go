package controllers

import (
	"github.com/gin-gonic/gin"
	"todo/handlers"
	"todo/middlewares"
)

func ListController(router gin.RouterGroup) {

	api := router.Group("/api/lists").Use(middlewares.Auth())
	{

		api.POST("/", handlers.CreateList)
		api.DELETE("/:id", handlers.DeleteList)
		api.GET("/", handlers.GetList)

	}
}
