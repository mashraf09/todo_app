package controllers

import (
	"github.com/gin-gonic/gin"
	"todo/handlers"
)

func UserController(router gin.RouterGroup) {

	api := router.Group("/api/users")
	{
		api.POST("/signup", handlers.RegisterUser)
		api.POST("/verify", handlers.VerifyUser)
		api.POST("/login", handlers.LoginUser)
		api.POST("/forgot_password", handlers.ForgotPassword)
		api.POST("/reset_password", handlers.ResetPassword)

	}

}
