package controllers

import (
	"github.com/gin-gonic/gin"
	"todo/handlers"
	"todo/middlewares"
)

func TaskController(router gin.RouterGroup) {

	api := router.Group("/api/tasks").Use(middlewares.Auth())
	{
		api.POST("/", handlers.CreateTask)
		api.DELETE("/:id", handlers.DeleteTask)
		api.PUT("/:id", handlers.UpdateTask)

	}
}
