package controllers

import (
	"github.com/gin-gonic/gin"
	"todo/handlers"
	"todo/middlewares"
)

func ReportController(router gin.RouterGroup) {

	api := router.Group("/api/reports").Use(middlewares.Auth())
	{

		api.GET("/count_stats", handlers.CountStats)
		api.GET("/completion_average", handlers.AverageTasksCompletedPerDay)
		api.GET("/delayed_tasks", handlers.DelayedTasks)
		api.GET("/max_completions_in_day", handlers.MaxTasksCompletedInSingleDay)
		api.GET("/tasks_creation_stats", handlers.TasksCreationStats)
		api.GET("/similar_tasks", handlers.SimilarTaskStats)

	}

}
