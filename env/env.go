package env

import (
	"fmt"
	"github.com/joho/godotenv"
	"os"
)

type envFile struct {
	ServerHost string
	SecreteKey string
	ServerPort string
	DbHost     string
	DbUsername string
	DbPassword string
	DbName     string
	DbPort     string
	DbSSLMode  string

	SmtpAccount       string
	SmtpPassword      string
	SmtpHost          string
	SmtpPort          string
	AccountVerifyLink string
	ResetPasswordLink string

	AttachmentUploadPath string

	OauthRedirectUrl   string
	OauthClientID      string
	OauthClientSecrete string
	OauthTokenInfoUrl  string

	CadenceHostPort     string
	CadenceService      string
	CadenceClient       string
	CadenceDomain       string
	CadenceTaskListName string
}

var Env *envFile

func init() {
	if err := godotenv.Load(); err != nil {
		fmt.Println(err.Error())
		fmt.Println("Error loading .env file")
	}

	secreteKey := os.Getenv("SECRETE_KEY")
	serverHost := os.Getenv("SERVER_HOST")
	serverPort := os.Getenv("SERVER_PORT")
	dbHost := os.Getenv("DB_HOST")
	dbUsername := os.Getenv("DB_USERNAME")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbPort := os.Getenv("DB_PORT")
	dbsslmode := os.Getenv("DB_SSL_MODE")

	smtpAccount := os.Getenv("SMTP_ACCOUNT")
	smtpPassword := os.Getenv("SMTP_PASSWORD")
	smtpHost := os.Getenv("SMTP_HOST")
	smtpPort := os.Getenv("SMTP_PORT")

	accountVerifyLink := os.Getenv("ACCOUNT_VERIFICATION_LINK")
	resetPasswordLink := os.Getenv("RESET_PASSWORD_LINK")
	attachmentUploadPath := os.Getenv("ATTACHMENTS_UPLOAD_PATH")

	oauthredirecturl := os.Getenv("OAUTH_REDIRECT_URL")
	oauthclientid := os.Getenv("CLIENT_ID")
	oauthclientsecrete := os.Getenv("CLIENT_SECRETE")

	oauthtokeninfourl := os.Getenv("OAUTH_TOKEN_INFO_URL")

	cadencehostport := os.Getenv("Cadence_Host_Port")
	cadenceclient := os.Getenv("Cadence_ClientName")
	cadenceservice := os.Getenv("Cadence_Service")

	cadencedomain := os.Getenv("Cadence_Domain")
	cadencetasklistname := os.Getenv("Cadence_TaskListName")

	Env = &envFile{

		ServerHost: serverHost,
		ServerPort: serverPort,
		DbHost:     dbHost,
		DbUsername: dbUsername,
		DbPassword: dbPassword,
		DbName:     dbName,
		DbPort:     dbPort,
		DbSSLMode:  dbsslmode,

		SmtpAccount:  smtpAccount,
		SmtpPassword: smtpPassword,
		SmtpHost:     smtpHost,
		SmtpPort:     smtpPort,

		AccountVerifyLink: accountVerifyLink,
		ResetPasswordLink: resetPasswordLink,

		SecreteKey:           secreteKey,
		AttachmentUploadPath: attachmentUploadPath,

		OauthClientID:      oauthclientid,
		OauthRedirectUrl:   oauthredirecturl,
		OauthClientSecrete: oauthclientsecrete,
		OauthTokenInfoUrl:  oauthtokeninfourl,

		CadenceClient:       cadenceclient,
		CadenceHostPort:     cadencehostport,
		CadenceService:      cadenceservice,
		CadenceDomain:       cadencedomain,
		CadenceTaskListName: cadencetasklistname,
	}
}
