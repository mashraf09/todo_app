package backgroundTasks

import (
	"github.com/jasonlvhit/gocron"
	log "github.com/sirupsen/logrus"
	database "todo/databases"
	"todo/utility"
)

func sendTaskReminder() {
	log.Info("Background Task - sendTaskReminder function called")
	var results []map[string]interface{}

	var email string
	var tasks string
	//var list []userTasks

	results, err := database.GetAllTasks()
	if err != nil {
		log.Info(err.Error())
		return
	}
	for i := 0; i < len(results); i++ {
		//fmt.Println(results[i]["email"])
		email = results[i]["email"].(string)
		tasks = results[i]["due_tasks"].(string)
		err := utility.TaskReminderMail(email, tasks)
		if err != nil {
			log.Info(err.Error())
			return
		}

	}

}

func ExecuteCronJob() {
	log.Info("ExecuteCronJob function Called")
	gocron.Every(utility.TaskReminderDays).Day().At(utility.TaskReminderTime).Do(sendTaskReminder)
	<-gocron.Start()
}
