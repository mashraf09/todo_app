package database

import (
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"todo/models"
	"todo/utility"
)

func CreateUser(user *models.User) error {

	record := Instance.Create(&user)

	return record.Error
}

func UpdateUser(user *models.User) error {
	record := Instance.Model(models.User{}).
		Where("email = ?", user.Email).
		Updates(&user)
	return record.Error
}

func VerifyUser(user *models.User, email string) error {
	record := Instance.Model(models.User{}).
		Where("email = ?", email).
		First(&user)
	return record.Error
}

func VerifyCredentials(email string, providedPassword string) (models.User, error) {

	var user models.User
	result := Instance.Where("Email = ?", email).First(&user)
	if result.Error != nil {
		return user, result.Error
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(providedPassword))
	if err != nil {
		return user, err
	}
	return user, nil

}

func GetUserID(email string) (uint, error) {

	var user models.User
	result := Instance.Where("Email = ?", email).First(&user)
	if result.Error != nil {
		return 0, result.Error
	}
	return user.ID, nil

}

func AssignToken(userToken *models.UserToken) error {
	record := Instance.Preload("users").
		Create(&userToken)
	return record.Error
}

func CreateTask(task *models.Task) error {
	var count int64
	result := Instance.Model(models.Task{}).
		Where("list_id =?", task.ListID).
		Count(&count)
	if result.Error != nil {
		return result.Error
	}

	if count < utility.TasksLimit {
		record := Instance.Create(&task)
		return record.Error
	}
	return fmt.Errorf("task Limit reached :50")

}

func CheckTaskList(email string) (models.List, error) {
	//var user models.User
	var list models.List

	result := Instance.Joins("JOIN users on users.id = lists.user_id").
		Where("users.email = ?", email).
		First(&list)
	if result.RowsAffected == 0 && result.Error == nil {
		return list, utility.NoRecordFound()
	}
	//fmt.Println(list.ID)
	return list, result.Error

}

func CreateList(list *models.List, email string) (string, string, error) {

	var localList models.List
	var user models.User
	result := Instance.Select("lists.user_id, lists.title").
		Joins("JOIN users on users.id = lists.user_id").
		Where("users.email = ?", email).
		First(&localList)

	if result.RowsAffected == 0 {

		userresult := Instance.Where("email = ?", email).First(&user)

		if userresult.Error != nil {

			return "", "", userresult.Error
		}
		list.UserID = user.ID
		result2 := Instance.Create(&list)
		if result2.Error != nil {
			return "", "", result2.Error
		}

		return "List Created", list.Title, nil
	}
	if result.Error != nil {
		return "", "", result.Error
	}
	resp := "List Already Exist"
	return resp, localList.Title, nil
}

func DeleteTask(task *models.Task) error {

	result := Instance.Unscoped().Delete(&task)
	if result.RowsAffected == 0 && result.Error == nil {
		return utility.NoRecordFound()
	}
	return result.Error
}
func DeleteList(list *models.List) error {

	result := Instance.Unscoped().Delete(&list)
	if result.RowsAffected == 0 && result.Error == nil {
		return utility.NoRecordFound()
	}
	return result.Error
}

func UpdateTask(task *models.Task) error {
	result := Instance.Where("id=?", task.ID).
		Updates(&task)

	if result.RowsAffected == 0 && result.Error == nil {
		return utility.NoRecordFound()
	}
	return result.Error

}

func ViewList(tasks *[]models.Task, email string) error {

	//var user models.User
	Instance.Preload("users, lists")
	result := Instance.Table("tasks").
		Where("users.email =?", email).
		Joins("JOIN lists on tasks.list_id=lists.id JOIN  users on lists.user_id=users.id ").
		Find(&tasks)
	if result.RowsAffected == 0 && result.Error == nil {
		return utility.NoRecordFound()
	}
	return result.Error

}

func CheckTask(taskID uint) error {
	result := Instance.Where("id=?", taskID).
		Find(&models.Task{})
	if result.RowsAffected == 0 && result.Error == nil {
		return utility.NoRecordFound()
	}
	return result.Error
}

func UploadAttachment(taskAttachment *[]models.TaskAttachments, size int) error {
	result := Instance.CreateInBatches(&taskAttachment, size)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func DownloadAttachment(taskAttachment *models.TaskAttachments, taskID uint) error {
	result := Instance.Where("id = ?", taskID).
		Find(&taskAttachment)
	if result.RowsAffected == 0 && result.Error == nil {
		return utility.NoRecordFound()
	}
	return result.Error
}

func CountStats(UserID uint) ([]map[string]interface{}, error) {
	var rows []map[string]interface{}

	result := Instance.Table("tasks").
		Select("tasks.completion_status as status, COUNT(tasks.completion_status) as count").
		Where("lists.user_id =?", UserID).
		Joins("JOIN lists on tasks.list_id = lists.id").
		Group("completion_status").Find(&rows)
	if result.Error != nil {
		return rows, result.Error
	}
	return rows, nil
}
func AverageTasksCompletedPerDay(UserID uint) (map[string]interface{}, error) {
	var rows map[string]interface{}

	result := Instance.Table("tasks").
		Select("coalesce((count(tasks.id)/NULLIF(extract(day From users.created_at-now()),0)),sum(1)/1) as daily_average").
		Where("users.id =? AND tasks.completion_status=?", UserID, "true").
		Joins("JOIN lists on tasks.list_id = lists.id JOIN users on lists.user_id=users.id").
		Group("users.created_at").
		Find(&rows)
	if result.Error != nil {
		return rows, result.Error
	}
	return rows, nil
}

func DelayedTasks(UserID uint) (map[string]interface{}, error) {

	var rows map[string]interface{}

	result := Instance.Table("tasks").
		Select("count(1)").
		Where("lists.user_id =? AND ((tasks.completion_at > tasks.due_at AND  tasks.completion_status = 'true') OR (tasks.due_at < now() AND tasks.completion_status = 'false'))", UserID).
		Joins("JOIN lists on tasks.list_id = lists.id").
		Find(&rows)
	if result.Error != nil {
		return rows, result.Error
	}
	return rows, nil
}
func MaxTasksCompletedInSingleDay(UserID uint) ([]map[string]interface{}, error) {
	var rows []map[string]interface{}

	subQuery := Instance.Select("date(tasks.completion_at) as date,(count(tasks.id)) as mycount").
		Table("tasks").
		Where("lists.user_id =? AND completion_status=?", UserID, "true").
		Joins("JOIN lists on tasks.list_id = lists.id").
		Group("date(tasks.completion_at)").
		Order("mycount desc")

	result := Instance.Select("*").Table("(?) as u ", subQuery).Limit(1).Find(&rows)
	if result.Error != nil {
		return rows, result.Error
	}
	return rows, nil
}
func TasksCreationStats(UserID uint) ([]map[string]interface{}, error) {
	var rows []map[string]interface{}

	result := Instance.Select("to_char(date(tasks.created_at),'day') as day, count(1)").
		Table("tasks").
		Where("lists.user_id = ?", UserID).
		Joins("JOIN lists on tasks.list_id = lists.id").
		Group("date(tasks.created_at)").
		Order("day desc").Find(&rows)

	if result.Error != nil {
		return rows, result.Error
	}
	return rows, nil
}
func SimilarTaskStats(UserID uint) ([]string, error) {
	var rows []string

	result := Instance.Select("tasks.title").
		Table("tasks").
		Where("lists.user_id = ?", UserID).
		Joins("JOIN lists on tasks.list_id = lists.id").
		Order("title desc").Find(&rows)

	if result.Error != nil {
		return rows, result.Error
	}
	return rows, nil
}

func GetAllTasks() ([]map[string]interface{}, error) {
	var rows []map[string]interface{}
	//var rows interface{}

	result := Instance.Select("users.email,array_to_string( array_agg( tasks.title), '--' ) as due_tasks").
		Table("users").
		Where("date(tasks.due_at) = current_date").
		Joins("JOIN lists on users.id = lists.user_id JOIN tasks on lists.id = tasks.list_id").
		Group("users.email").
		Find(&rows)

	if result.Error != nil {
		return rows, result.Error
	}
	return rows, nil
}

func IsUserExist(email string) (models.User, error) {
	var user models.User
	result := Instance.Where("users.email = ?", email).First(&user)
	if result.Error != nil {
		return user, result.Error
	}

	return user, nil

}

func UpdateToken(userToken *models.UserToken) error {
	record := Instance.Model(models.UserToken{}).
		Where("user_tokens.user_id=?", userToken.UserID).Updates(&userToken)
	return record.Error

}

func VerifyToken(token string) (models.User, error) {
	var user models.User
	record := Instance.Table("users").Where("user_tokens.token=?", token).
		Joins("JOIN user_tokens ON users.id=user_tokens.user_id").First(&user)

	if record.Error != nil {
		return user, record.Error
	}

	return user, nil

}
