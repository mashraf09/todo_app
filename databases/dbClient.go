package database

import (
	log "github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"todo/models"
)

var Instance *gorm.DB
var dbError error

func Connect(connectionString string) {
	Instance, dbError = gorm.Open(postgres.Open(connectionString), &gorm.Config{})
	if dbError != nil {
		//log.Fatal(dbError)
		log.Error(dbError.Error())
		panic("Cannot connect to DB")
	}
	log.Info("Connected to Database!")
}
func Migrate() {
	Instance.AutoMigrate(&models.User{}, &models.UserToken{}, &models.List{}, &models.Task{}, &models.TaskAttachments{})
	log.Info("Database Migration Completed!")

}
