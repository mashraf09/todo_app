# Todo Application


## Project Summary

One of the most common tools used by professionals all over the world to capture tasks is a To Do list. They come in all shapes, forms, and features. This project you provide Backend of a SAAS application that allows its users to build and maintain a To Do list.  ALso added support for OAuth 2 login.

One user can only have one To Do list. Each list can have up to 50 tasks. A task consist of following properties: creation date time, title, description, file attachments (e.g. an image), due date time, completion status (true/false), completion date time.

A user should not be allowed to access tasks created by any other user."
