package utility

import (
	log "github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"strconv"
	"todo/env"
)

func DeleteAttachments(taskID uint) error {
	log.Info("DeleteAttachment Function Called")
	x := int(taskID)
	str := strconv.Itoa(x)
	taskDir := "task_" + str
	uploadDir := env.Env.AttachmentUploadPath
	dirPath := filepath.Join(uploadDir, string(taskDir))
	err := os.RemoveAll(dirPath)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	log.Info("DeleteAttachment Successful")
	return nil
}
