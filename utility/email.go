package utility

import (
	"github.com/pborman/uuid"
	log "github.com/sirupsen/logrus"
	"go.uber.org/cadence/client"
	"golang.org/x/net/context"
	"strings"
	"time"
	"todo/cadenceWorkflows"
	"todo/env"
)

func AccountVerificationMail(toEmail string, tokenStr string) error {
	log.Info("AccountVerificationMail Function Called")
	from := env.Env.SmtpAccount
	password := env.Env.SmtpPassword

	toEmailAddress := toEmail
	to := []string{toEmailAddress}

	host := env.Env.SmtpHost
	port := env.Env.SmtpPort

	//address := host + ":" + port
	verifyLink := env.Env.AccountVerifyLink

	subject := "Subject: Account Verification\n"
	body := "Please verify your account using token given below.\n" +
		"Token:\n" + tokenStr + "\n\n" +
		"Use this link to verify your account\n" + verifyLink
	//message := []byte(subject + body)
	//
	//auth := smtp.PlainAuth("", from, password, host)
	//
	//err := smtp.SendMail(address, auth, from, to, message)
	//if err != nil {
	//	log.Error(err.Error())
	//	panic(err)
	//}
	workflowClient := client.NewClient(cadenceWorkflows.BuildCadenceClient(), "SimpleDomain", &client.Options{})
	workflowOptions := client.StartWorkflowOptions{
		ID:                              "todo_app" + uuid.New(),
		TaskList:                        cadenceWorkflows.ApplicationName,
		ExecutionStartToCloseTimeout:    1 * time.Minute,
		DecisionTaskStartToCloseTimeout: 1 * time.Minute,
	}
	_, err := workflowClient.StartWorkflow(
		context.Background(),
		workflowOptions,
		cadenceWorkflows.EmailNotifyWorkflow,
		to, from, subject, body, host, password, port)
	if err != nil {
		log.Error(err.Error())
		return err
	}

	log.Info("Account verification email sent successfully")
	return nil
}

func ResetPasswordMail(ctx context.Context, toEmail string, tokenStr string) error {

	log.Info("ResetPasswordMail Function Called")
	from := env.Env.SmtpAccount
	password := env.Env.SmtpPassword

	toEmailAddress := toEmail
	to := []string{toEmailAddress}

	host := env.Env.SmtpHost
	port := env.Env.SmtpPort

	//address := host + ":" + port
	resetPasswordLink := env.Env.ResetPasswordLink

	subject := "Subject: Password Reset Email\n"
	body := "Please reset your password using token given below.\n" +
		"Token:\n" + tokenStr + "\n\n" +
		"Use this link to reset your password\n" + resetPasswordLink
	//message := []byte(subject + body)

	//auth := smtp.PlainAuth("", from, password, host)

	//err := smtp.SendMail(address, auth, from, to, message)
	//replace this line and call workflow here instead of sendEmail pass email related data

	workflowClient := client.NewClient(cadenceWorkflows.BuildCadenceClient(), "SimpleDomain", &client.Options{})
	workflowOptions := client.StartWorkflowOptions{
		ID:                              "todo_app" + uuid.New(),
		TaskList:                        cadenceWorkflows.ApplicationName,
		ExecutionStartToCloseTimeout:    1 * time.Minute,
		DecisionTaskStartToCloseTimeout: 1 * time.Minute,
	}
	_, err := workflowClient.StartWorkflow(
		ctx,
		workflowOptions,
		cadenceWorkflows.EmailNotifyWorkflow,
		to, from, subject, body, host, password, port)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	log.Info("Reset password email sent successfully")
	return nil
}

func TaskReminderMail(toEmail string, tasksList string) error {
	log.Info("TaskReminderMail Function Called")
	tokenizedTasks := strings.ReplaceAll(tasksList, "--", "\n")
	from := env.Env.SmtpAccount
	password := env.Env.SmtpPassword

	toEmailAddress := toEmail
	to := []string{toEmailAddress}

	host := env.Env.SmtpHost
	port := env.Env.SmtpPort

	//address := host + ":" + port
	//resetPasswordLink := env.Env.ResetPasswordLink

	subject := "Subject: Reminder - Tasks due today \n"
	body := "Dear User,\n" +
		"Following tasks are due today:\n" + "\n\n" +
		tokenizedTasks
	//message := []byte(subject + body)
	//
	//auth := smtp.PlainAuth("", from, password, host)
	//
	//err := smtp.SendMail(address, auth, from, to, message)
	//if err != nil {
	//	log.Error(err.Error())
	//	return err
	//}
	workflowClient := client.NewClient(cadenceWorkflows.BuildCadenceClient(), "SimpleDomain", &client.Options{})
	workflowOptions := client.StartWorkflowOptions{
		ID:                              "todo_app" + uuid.New(),
		TaskList:                        cadenceWorkflows.ApplicationName,
		ExecutionStartToCloseTimeout:    1 * time.Minute,
		DecisionTaskStartToCloseTimeout: 1 * time.Minute,
	}
	_, err := workflowClient.StartWorkflow(
		context.Background(),
		workflowOptions,
		cadenceWorkflows.EmailNotifyWorkflow,
		to, from, subject, body, host, password, port)
	if err != nil {
		log.Error(err.Error())
		return err
	}
	log.Info("Task Reminder email sent successfully")
	return nil
}
