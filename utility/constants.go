package utility

import "time"

var TokenExpiryTime time.Time = time.Now().Add(1 * time.Hour)

const TasksLimit int64 = 15

const TaskReminderDays uint64 = 1
const TaskReminderTime string = "00:00:00"

var OauthScopes = []string{"https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"}

var CacheExpireyTime time.Duration = 15 * time.Minute
var CacheCleanUpInterval time.Duration = 10 * time.Minute
