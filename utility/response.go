package utility

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"todo/models"
)

func BuildResponse(context *gin.Context, statusCode int, status string, err string, data interface{}) {
	response := models.Response{
		Status: status,
		Error:  err,
		Data:   data,
	}
	context.JSON(statusCode, response)
	if status == "error" {
		context.Abort()
	}

}
func NoRecordFound() error {
	return fmt.Errorf("no record found")
}

func RecordAlreadyExist(str string) error {
	return fmt.Errorf(str + " Already Exist")
}
